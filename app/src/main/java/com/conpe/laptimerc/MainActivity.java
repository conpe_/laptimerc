package com.conpe.laptimerc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // タイトルバー生成
        setRootActionBar();

    }


    // 画面生成時(戻ってきたとき含む)の処理
    @Override
    protected void onResume(){
        super.onResume();

    }


    // 画面を離れるときの処理
    @Override
    protected void onPause() {
        super.onPause();

    }


    // タイトルバー生成
    protected void setRootActionBar(){

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

    }

    // オプションメニュー生成
    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        return true;
    }

    // オプションメニュー選択時の動作
    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){
            case R.id.menu_viewlog:
                Log.d("debug", "ログ表示");
                Toast.makeText(MainActivity.this,"ログ表示",Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_setting:
                Log.d("debug", "デバイス設定");
                Toast.makeText(MainActivity.this,"デバイス設定",Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplication(), com.conpe.laptimerc.SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_help:
                Log.d("debug", "ヘルプ");
                Toast.makeText(MainActivity.this,"ヘルプ",Toast.LENGTH_SHORT).show();

                break;
        }

        return true;
    }
}