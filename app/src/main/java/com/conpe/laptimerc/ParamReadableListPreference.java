package com.conpe.laptimerc;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import androidx.preference.ListPreference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;
import androidx.preference.PreferenceViewHolder;

import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ParamReadableListPreference extends ListPreference  {

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ParamReadableListPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        //this.setLayoutResource(R.layout.param_readable_list_preference);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ParamReadableListPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //this.setLayoutResource(R.layout.param_readable_list_preference);
    }

    public ParamReadableListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        //this.setLayoutResource(R.layout.param_readable_list_preference);
    }

    public ParamReadableListPreference(Context context) {
        super(context);
        //this.setLayoutResource(R.layout.param_readable_list_preference);
    }

    @Override
    //public boolean onPreferenceStartScreen (PreferenceFragmentCompat caller, PreferenceScreen pref){
    public void onBindViewHolder(PreferenceViewHolder holder){
        super.onBindViewHolder(holder);

        //this.setLayoutResource(R.layout.param_readable_list_preference);
        //TextView textView = (TextView)holder.findViewById(R.id.valueView);

        //this.setSummary("summary");

        //textView.setText("setTxt"); //getEntry()
        //Log.d("debug", "dialog_title : " + getDialogTitle().toString());
    }


/*
    @Override
    protected View onCreateView(ViewGroup parent) {
        setWidgetLayoutResource(R.layout.param_readable_list_preference);
        return super.onCreateView(parent);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        TextView textView = (TextView) view.findViewById(R.id.valueView);
        textView.setText(getEntry());
    }
    */
}